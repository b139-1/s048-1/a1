import 'bootstrap/dist/css/bootstrap.min.css';
import {Fragment} from "react"
import {BrowserRouter, Switch, Route} from "react-router-dom"
import {Container} from "react-bootstrap"
import AppNavbar from "./components/AppNavbar.js"
import Home from "./pages/Home.js"
import Courses from "./pages/Courses.js"
import Register from "./pages/Register.js"
import Login from './pages/Login.js'
import Logout from "./pages/Logout.js"
import NotFound from "./pages/NotFound.js"

export default function App() {

  return (
    <BrowserRouter>
      <AppNavbar/>
      <Switch>
        <Route exact path="/" component={Home}/>
        <Route exact path="/courses" component={Courses}/>
        <Route exact path="/register" component={Register}/>
        <Route exact path="/login" component={Login}/>
        <Route exact path="/logout" component={Logout}/>
        <Route component={NotFound}/>
      </Switch>
    </BrowserRouter>
  )
}