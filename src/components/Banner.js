import {Container, Row, Col, Button} from "react-bootstrap"

export default function Banner({bannerProp}) {
	
	const {title, message, option} = bannerProp

	return (
		<Container fluid className="m-4">
			<Row>
				<Col>
					<div className="jumbotron">
						<h1>{title}</h1>
						<p>{message}</p>
						<Button variant="primary">{option}</Button>
					</div>
				</Col>
			</Row>
		</Container>
	)
}