const courseData = [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing, elit. Maiores adipisci, ducimus totam eius. Error maiores hic, nihil autem eos deleniti reprehenderit, eum voluptas minima pariatur consequatur, quis aspernatur.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Java - Springboot",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing, elit. Maiores adipisci, ducimus totam eius. Error maiores hic, nihil autem eos deleniti reprehenderit, eum voluptas minima pariatur consequatur, quis aspernatur.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Node - Express",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing, elit. Maiores adipisci, ducimus totam eius. Error maiores hic, nihil autem eos deleniti reprehenderit, eum voluptas minima pariatur consequatur, quis aspernatur.",
		price: 55000,
		onOffer: true
	}
]

export default courseData