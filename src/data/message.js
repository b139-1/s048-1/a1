const message = [
	
	{
		title: "Zuitt Coding Bootcamp",
		message: "Opportunities for everyone, everywhere",
		option: "Enroll now!"
	},
	{
		title: "Page Not Found!",
		message: "Apparently, we couldn't find what you're looking for",
		option: "Homepage"
	}
	
]

export default message