import {Fragment} from "react"
import courseData from "./../data/courseData.js"
import CourseCard from "./../components/CourseCard.js"

export default function Courses() {
	console.log(courseData)
	const courses = courseData.map( (element) => {
		return (<CourseCard key={element.id} courseProp={element}/>)
	})
	return (
		<Fragment>
			{courses}
		</Fragment>
	)
}