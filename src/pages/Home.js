import {Fragment} from "react"

import Banner from "./../components/Banner.js"
import Highlights from "./../components/Highlights.js"
import message from "./../data/message.js"

export default function Home() {
	return (
		<Fragment>
			<Banner bannerProp={message[0]}/>
			<Highlights/>
		</Fragment>
	)
}