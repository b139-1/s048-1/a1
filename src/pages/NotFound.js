import {Fragment} from 'react'
import Banner from "./../components/Banner.js"
import message from "./../data/message.js"

export default function NotFound() {
	return (
		<Fragment>
			<Banner bannerProp={message[1]}/>
		</Fragment>
	)
}